// import 'package:flutter/material.dart';
// import 'package:go_router/go_router.dart';
//
// class ScaffoldWithNavbar extends StatelessWidget {
//   const ScaffoldWithNavbar(this.navigationShell, {super.key});
//
//   /// The navigation shell and container for the branch Navigators.
//   final StatefulNavigationShell navigationShell;
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: navigationShell,
//       bottomNavigationBar: NavigationBar(
//         // backgroundColor: Colors.black,
//         // type: BottomNavigationBarType.fixed,
//         // fixedColor: Colors.red,
//         selectedIndex: navigationShell.currentIndex,
//         onDestinationSelected: _onTap,
//
//         // currentIndex: navigationShell.currentIndex,
//
//         destinations: const [
//           NavigationDestination(icon: Icon(Icons.home_outlined), label: 'Home'),
//           NavigationDestination(icon: Icon(Icons.mood), label: 'Tracker'),
//           NavigationDestination(
//               icon: Icon(Icons.info_outline), label: 'Resource'),
//           NavigationDestination(
//               icon: Icon(Icons.settings_outlined), label: 'Settings'),
//         ],
//       ),
//     );
//   }
//
//   void _onTap(index) {
//     // setState(() {
//     //  widget.navigationShell.goBranch(index,
//     //  initialLocation: index==widget.navigationShell.currentIndex) ;
//     // });
//     navigationShell.goBranch(
//       index,
//       // A common pattern when using bottom navigation bars is to support
//       // navigating to the initial location when tapping the item that is
//       // already active. This example demonstrates how to support this behavior,
//       // using the initialLocation parameter of goBranch.
//       initialLocation: index == navigationShell.currentIndex,
//     );
//   }
// }
