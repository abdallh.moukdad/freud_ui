import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mood_tracking_freud_ui/core/styles.dart';

import '../constants/constants.dart';
// import 'package:mood_tracking_freud_ui/features/assessment/goal_for_today/presentation/view/widgets/today_goal_app_bar.dart';

class GoalAppBar extends StatelessWidget {
  const GoalAppBar({super.key, this.pageNumber});
final pageNumber;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 15.0),
      child: Row(
        children: [
          GestureDetector(child: ButtonContainer()),
          SizedBox(
            width: 15,
          ),
          Text(
            "Assessment",
            style: Styles.AppBarTextTheme,
          ),
          SizedBox(
            width: 120,
          ),
          TagMaster(pageNumber: pageNumber,),
        ],
      ),
    );
  }
}
class ButtonContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 48,
          height: 48,
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              side: BorderSide(width: 1, color: Color(0xFF4E3321)),
              borderRadius: BorderRadius.circular(1234),
            ),
          ),
          child: SvgPicture.asset(
            KBackButtonPath,
            width: 20,
            height: 10,
            fit: BoxFit.scaleDown,
          ),
        ),
      ],
    );
  }
}

class TagMaster extends StatelessWidget {
  final pageNumber;

  const TagMaster({super.key,@required this.pageNumber});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          // width: 57,
          height: 28,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
          clipBehavior: Clip.antiAlias,
          decoration: ShapeDecoration(
            color: Color(0xFFE8DCD8),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(32),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                pageNumber==null ?'0 of 14':'$pageNumber of 14',
                // '$pageNumber of 14',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xFF926247),
                  fontSize: 14,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.w700,
                  height: 0,
                  letterSpacing: -0.03,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

