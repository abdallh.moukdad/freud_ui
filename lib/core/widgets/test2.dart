import 'package:flutter/material.dart';

class Frame2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          SizedBox(
            height: 100,
          ),
          Container(
            width: 343,
            height: 596,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 343,
                  child: Text(
                    'How would you rate your sleep quality?',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFF4E3321),
                      fontSize: 30,
                      fontFamily: 'Urbanist',
                      fontWeight: FontWeight.w800,
                      height: 0.04,
                      letterSpacing: -0.30,
                    ),
                  ),
                ),
                const SizedBox(height: 40),
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 343,
                        padding: const EdgeInsets.symmetric(vertical: 24),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    width: double.infinity,
                                    child: Text(
                                      'Excellent',
                                      style: TextStyle(
                                        color: Color(0xFFACA8A5),
                                        fontSize: 18,
                                        fontFamily: 'Urbanist',
                                        fontWeight: FontWeight.w800,
                                        height: 0,
                                        letterSpacing: -0.07,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 8),
                                  Container(
                                    width: double.infinity,
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          width: 16,
                                          height: 16,
                                          padding: const EdgeInsets.all(1.33),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [],
                                          ),
                                        ),
                                        const SizedBox(width: 4),
                                        Text(
                                          '7-9 HOURS',
                                          style: TextStyle(
                                            color: Color(0xFFC9C7C5),
                                            fontSize: 12,
                                            fontFamily: 'Urbanist',
                                            fontWeight: FontWeight.w800,
                                            height: 0,
                                            letterSpacing: 1.20,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: 48,
                              height: 48,
                              decoration: ShapeDecoration(
                                color: Color(0xFF9BB067),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                              child: Stack(children: []),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: 343,
                        padding: const EdgeInsets.symmetric(vertical: 24),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    width: double.infinity,
                                    child: Text(
                                      'Good',
                                      style: TextStyle(
                                        color: Color(0xFFACA8A5),
                                        fontSize: 18,
                                        fontFamily: 'Urbanist',
                                        fontWeight: FontWeight.w800,
                                        height: 0,
                                        letterSpacing: -0.07,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 8),
                                  Container(
                                    width: double.infinity,
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          width: 16,
                                          height: 16,
                                          padding: const EdgeInsets.all(1.33),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [],
                                          ),
                                        ),
                                        const SizedBox(width: 4),
                                        Text(
                                          '6-7 HOURS',
                                          style: TextStyle(
                                            color: Color(0xFFC9C7C5),
                                            fontSize: 12,
                                            fontFamily: 'Urbanist',
                                            fontWeight: FontWeight.w800,
                                            height: 0,
                                            letterSpacing: 1.20,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: 48,
                              height: 48,
                              decoration: ShapeDecoration(
                                color: Color(0xFFFFCE5B),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                              child: Stack(children: []),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: 343,
                        padding: const EdgeInsets.symmetric(vertical: 24),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    width: double.infinity,
                                    child: Text(
                                      'Fair',
                                      style: TextStyle(
                                        color: Color(0xFFACA8A5),
                                        fontSize: 18,
                                        fontFamily: 'Urbanist',
                                        fontWeight: FontWeight.w800,
                                        height: 0,
                                        letterSpacing: -0.07,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 8),
                                  Container(
                                    width: double.infinity,
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          width: 16,
                                          height: 16,
                                          padding: const EdgeInsets.all(1.33),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [],
                                          ),
                                        ),
                                        const SizedBox(width: 4),
                                        Text(
                                          '5 HOURS',
                                          style: TextStyle(
                                            color: Color(0xFFC9C7C5),
                                            fontSize: 12,
                                            fontFamily: 'Urbanist',
                                            fontWeight: FontWeight.w800,
                                            height: 0,
                                            letterSpacing: 1.20,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: 48,
                              height: 48,
                              decoration: ShapeDecoration(
                                color: Color(0xFFBFA090),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                              child: Stack(children: []),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: 343,
                        padding: const EdgeInsets.symmetric(vertical: 24),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    width: double.infinity,
                                    child: Text(
                                      'Poor',
                                      style: TextStyle(
                                        color: Color(0xFF4E3321),
                                        fontSize: 18,
                                        fontFamily: 'Urbanist',
                                        fontWeight: FontWeight.w800,
                                        height: 0,
                                        letterSpacing: -0.07,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 8),
                                  Container(
                                    width: double.infinity,
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          width: 16,
                                          height: 16,
                                          padding: const EdgeInsets.all(1.33),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [],
                                          ),
                                        ),
                                        const SizedBox(width: 4),
                                        Text(
                                          '3-4 HOURS',
                                          style: TextStyle(
                                            color: Color(0xFF4E3321),
                                            fontSize: 12,
                                            fontFamily: 'Urbanist',
                                            fontWeight: FontWeight.w800,
                                            height: 0,
                                            letterSpacing: 1.20,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: 48,
                              height: 48,
                              decoration: ShapeDecoration(
                                color: Color(0xFFEC7D1C),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                              child: Stack(children: []),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: 343,
                        padding: const EdgeInsets.symmetric(vertical: 24),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    width: double.infinity,
                                    child: Text(
                                      'Worst',
                                      style: TextStyle(
                                        color: Color(0xFFACA8A5),
                                        fontSize: 18,
                                        fontFamily: 'Urbanist',
                                        fontWeight: FontWeight.w800,
                                        height: 0,
                                        letterSpacing: -0.07,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 8),
                                  Container(
                                    width: double.infinity,
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          width: 16,
                                          height: 16,
                                          padding: const EdgeInsets.all(1.33),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [],
                                          ),
                                        ),
                                        const SizedBox(width: 4),
                                        Text(
                                          '<3 HOURS',
                                          style: TextStyle(
                                            color: Color(0xFFC9C7C5),
                                            fontSize: 12,
                                            fontFamily: 'Urbanist',
                                            fontWeight: FontWeight.w800,
                                            height: 0,
                                            letterSpacing: 1.20,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: 48,
                              height: 48,
                              decoration: ShapeDecoration(
                                color: Color(0xFFA694F5),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                              child: Stack(children: []),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
