import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: MoveableWidget(),
        ),
      ),
    );
  }
}

class MoveableWidget extends StatefulWidget {
  @override
  _MoveableWidgetState createState() => _MoveableWidgetState();
}

class _MoveableWidgetState extends State<MoveableWidget> {
  double x = 0;
  double y = 0;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanUpdate: (details) {
        setState(() {
          x += details.delta.dx;
          y += details.delta.dy;
        });
      },
      child: Stack(
        children: [
          Positioned(
            left: x,
            top: y,
            child: Container(
              width: 100,
              height: 100,
              color: Colors.blue,
            ),
          ),
          Positioned(
            left: x + 120,
            top: y,
            child: Container(
              width: 100,
              height: 100,
              color: Colors.red,
            ),
          ),
          Positioned(
            left: x - 120,
            top: y,
            child: Container(
              width: 100,
              height: 100,
              color: Colors.green,
            ),
          ),
          Positioned(
            left:x+200,
            top: y+200,

            child: Container(width: 500, height: 550, child: Frame()),
          ),
        ],
      ),
    );
  }
}

class Frame extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 422,
          height: 525,
          child: Stack(
            children: [
              Positioned(
                left: 0,
                top: 103,
                child: Container(
                  width: 422,
                  height: 422,
                  decoration: ShapeDecoration(
                    shape: OvalBorder(
                      side: BorderSide(
                        width: 160,
                        strokeAlign: BorderSide.strokeAlignOutside,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 0,
                top: 103,
                child: Container(
                  width: 422,
                  height: 422,
                  decoration: ShapeDecoration(
                    shape: OvalBorder(
                      side: BorderSide(
                        width: 160,
                        strokeAlign: BorderSide.strokeAlignOutside,
                        color: Color(0xFFFFCE5B),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 0,
                top: 103,
                child: Container(
                  width: 422,
                  height: 422,
                  decoration: ShapeDecoration(
                    shape: OvalBorder(
                      side: BorderSide(
                        width: 160,
                        strokeAlign: BorderSide.strokeAlignOutside,
                        color: Color(0xFFEC7D1C),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 0,
                top: 103,
                child: Container(
                  width: 422,
                  height: 422,
                  decoration: ShapeDecoration(
                    shape: OvalBorder(
                      side: BorderSide(
                        width: 160,
                        strokeAlign: BorderSide.strokeAlignOutside,
                        color: Color(0xFF9BB067),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 0,
                top: 103,
                child: Container(
                  width: 422,
                  height: 422,
                  decoration: ShapeDecoration(
                    color: Colors.black,
                    shape: OvalBorder(
                      side: BorderSide(
                        width: 160,
                        strokeAlign: BorderSide.strokeAlignOutside,
                        color: Color(0xFFA694F5),
                      ),
                    ),
                  ),
                ),
              ),
              // Positioned(
              //   left: 0,
              //   top: 103,
              //   child: Container(
              //     width: 422,
              //     height: 422,
              //     decoration: ShapeDecoration(
              //       shape: OvalBorder(
              //         side: BorderSide(
              //           width: 160,
              //           strokeAlign: BorderSide.strokeAlignOutside,
              //           color: Color(0xFF926247),
              //         ),
              //       ),
              //     ),
              //   ),
              // ),
              // Positioned(
              //   left: 0,
              //   top: 103,
              //   child: Opacity(
              //     opacity: 0.15,
              //     child: Container(
              //       width: 422,
              //       height: 422,
              //       decoration: ShapeDecoration(
              //         shape: OvalBorder(
              //           side: BorderSide(
              //             width: 16,
              //             strokeAlign: BorderSide.strokeAlignOutside,
              //             color: Color(0xFF4E3321),
              //           ),
              //         ),
              //       ),
              //     ),
              //   ),
              // ),
              Positioned(
                left: 229,
                top: 134,
                child: Transform(
                  transform: Matrix4.identity()
                    ..translate(0.0, 0.0)
                    ..rotateZ(-3.14),
                  child: Container(
                    width: 37,
                    height: 56,
                    child: Stack(
                      children: [
                        Positioned(
                          left: 24,
                          top: 22,
                          child: Transform(
                            transform: Matrix4.identity()
                              ..translate(0.0, 0.0)
                              ..rotateZ(3.14),
                            child: Container(
                              width: 12,
                              height: 12,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 184,
                top: 0,
                child: Container(
                  width: 52.50,
                  height: 37.50,
                  child: Stack(children: []),
                ),
              ),
              Positioned(
                left: 339.50,
                top: 25,
                child: Transform(
                  transform: Matrix4.identity()
                    ..translate(0.0, 0.0)
                    ..rotateZ(0.52),
                  child: Container(
                    width: 60,
                    height: 45,
                    child: Stack(children: []),
                  ),
                ),
              ),
              Positioned(
                left: 29,
                top: 51,
                child: Transform(
                  transform: Matrix4.identity()
                    ..translate(0.0, 0.0)
                    ..rotateZ(-0.52),
                  child: Container(
                    width: 52.50,
                    height: 43.90,
                    child: Stack(children: []),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
