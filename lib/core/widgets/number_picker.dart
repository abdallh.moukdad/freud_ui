import 'dart:async';

import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';

class NumberPickerDemo extends StatefulWidget {
  @override
  _NumberPickerDemoState createState() => _NumberPickerDemoState();
}

class _NumberPickerDemoState extends State<NumberPickerDemo> {
  int _currentIntValue = 10;
  double _currentDoubleValue = 3.0;
  late NumberPicker integerNumberPicker;
  // late NumberPicker decimalNumberPicker;

  _handleValueChanged(num value) {
    if (value != null) {
      if (value is int) {
        setState(() => _currentIntValue = value);
      } else {
        setState(() => _currentDoubleValue = value.toDouble());
      }
    }
  }

  // _handleValueChangedExternally(num value) {
  //   if (value != null) {
  //     if (value is int) {
  //       setState(() => _currentIntValue = value);
  //       integerNumberPicker.animateInt(value);
  //     } else {
  //       setState(() => _currentDoubleValue = value);
  //       decimalNumberPicker.animateDecimalAndInteger(value);
  //     }
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    // integerNumberPicker = NumberPicker.integer(
    //   initialValue: _currentIntValue,
    //   minValue: 0,
    //   maxValue: 100,
    //   step: 10,
    //   onChanged: _handleValueChanged,
    // );
    integerNumberPicker = NumberPicker(
      minValue: 0,
      maxValue: 100,
      value: 20,
      onChanged: (int value) {},
    );
    //build number picker for decimal values
    // decimalNumberPicker = NumberPicker.decimal(
    //     initialValue: _currentDoubleValue,
    //     minValue: 1,
    //     maxValue: 5,
    //     decimalPlaces: 2,
    //     onChanged: _handleValueChanged);
    //scaffold the full homepage
    return Scaffold(
        appBar: AppBar(
          title: Text('Number Picker Demo'),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              integerNumberPicker,
              ElevatedButton(
                onPressed: () => _showIntegerDialog(),
                child: Text("Int Value: $_currentIntValue"),
                // color: Colors.grey[300],
              ),
              // decimalNumberPicker,
              // ElevatedButton(
              //   onPressed: () => _showDoubleDialog(),
              //   child: Text("Decimal Value: $_currentDoubleValue"),
              //   // color: Colors.pink[100],
              // ),
            ],
          ),
        ));
  }

  Future _showIntegerDialog() async {
    await showDialog<int>(
      context: context,
      builder: (BuildContext context) {
        // return NumberPickerDialog.integer(
        //   minValue: 0,
        //   maxValue: 100,
        //   step: 10,
        //   initialIntegerValue: _currentIntValue,
        //   title: Text("Pick a int value"),
        // );
        return integerNumberPicker;
      },
    ); //.then(_handleValueChangedExternally);
  }

// Future _showDoubleDialog() async {
//   await showDialog<double>(
//     context: context,
//     builder: (BuildContext context) {
//       return NumberPickerDialog.decimal(
//         minValue: 1,
//         maxValue: 5,
//         decimalPlaces: 2,
//         initialDoubleValue: _currentDoubleValue,
//         title: Text("Pick a decimal value"),
//       );
//     },
//   ).then(_handleValueChangedExternally);
// }
}
