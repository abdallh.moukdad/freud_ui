import 'package:flutter/material.dart';

class AssessmentText extends StatelessWidget {
  const AssessmentText({super.key, this.text});
  final text;
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: TextAlign.center,
      // overflow: TextOverflow.fade,
      style: TextStyle(
        color: Color(0xFF4E3321),
        fontSize: 30,
        fontFamily: 'Urbanist',
        fontWeight: FontWeight.w800,
        // height: 0.04,
        letterSpacing: -0.30,
      ),
    );
  }
}
