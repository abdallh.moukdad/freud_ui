import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Custom Shape Slider'),
        ),
        body: Center(
          child: CustomShapeSlider(),
        ),
      ),
    );
  }
}

class CustomShapeSlider extends StatefulWidget {
  @override
  _CustomShapeSliderState createState() => _CustomShapeSliderState();
}

class _CustomShapeSliderState extends State<CustomShapeSlider> {
  double _sliderValue = 0.5;

  @override
  Widget build(BuildContext context) {
    return SliderTheme(
      data: SliderThemeData(
        thumbColor: Colors.blue,
        activeTrackColor: Colors.blue,
        inactiveTrackColor: Colors.grey,
        trackHeight: 8.0,
        thumbShape: CustomThumbShape(),
      ),
      child: Slider(
        value: _sliderValue,
        onChanged: (value) {
          setState(() {
            _sliderValue = value;
          });
        },
      ),
    );
  }
}

class CustomThumbShape extends SliderComponentShape {
  final double thumbRadius = 12.0;

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size(thumbRadius * 2, thumbRadius * 2);
  }

  @override
  void paint(PaintingContext context, Offset center,
      {required Animation<double> activationAnimation,
      required Animation<double> enableAnimation,
      required bool isDiscrete,
      required TextPainter labelPainter,
      required RenderBox parentBox,
      required SliderThemeData sliderTheme,
      required TextDirection textDirection,
      required double value,
      required double textScaleFactor,
      required Size sizeWithOverflow}) {
    // TODO: implement paint
    final Canvas canvas = context.canvas;
    final Paint paint = Paint()
      ..color = sliderTheme.thumbColor!
      ..style = PaintingStyle.fill;

    canvas.drawCircle(center, thumbRadius, paint);
  }
// @override
// void paint(PaintingContext context, Offset center,
//     {required Animation<double> activationAnimation,
//       required Animation<double> enableAnimation,
//       required bool isDiscrete,
//       required TextPainter labelPainter,
//       required RenderBox parentBox,
//       required SliderThemeData sliderTheme,
//       required TextDirection textDirection,
//       required double value}) {
//   final Canvas canvas = context.canvas;
//   final Paint paint = Paint()
//     ..color = sliderTheme.thumbColor!
//     ..style = PaintingStyle.fill;
//
//   canvas.drawCircle(center, thumbRadius, paint);
// }
}

// class CustomSliderThumbCircle extends SliderComponentShape {
//   final double thumbRadius;
//   final double thumbHeight;
//
//   const CustomSliderThumbCircle({
//     required this.thumbRadius,
//     required this.thumbHeight,
//   });
//
//   @override
//   Size getPreferredSize(bool isEnabled, bool isDiscrete) {
//     return Size(thumbRadius, thumbHeight);
//   }
//
//   @override
//   void paint(PaintingContext context, Offset center,
//       {required Animation<double> activationAnimation,
//       required Animation<double> enableAnimation,
//       required bool isDiscrete,
//       required TextPainter labelPainter,
//       required RenderBox parentBox,
//       required SliderThemeData sliderTheme,
//       required TextDirection textDirection,
//       required double value,
//       required double textScaleFactor,
//       required Size sizeWithOverflow}) {
//     final Canvas canvas = context.canvas;
//
//     final Paint paint = Paint()
//       ..color = Colors.blue
//       ..style = PaintingStyle.fill;
//
//     canvas.drawCircle(center, thumbRadius, paint);
//   }
// }
//
// class CustomSlider extends StatefulWidget {
//   @override
//   _CustomSliderState createState() => _CustomSliderState();
// }
//
// class _CustomSliderState extends State<CustomSlider> {
//   double _value = 0.0;
//
//   @override
//   Widget build(BuildContext context) {
//     return Slider(
//       value: _value,
//       onChanged: (double value) {
//         setState(() {
//           _value = value;
//         });
//       },
//       min: 0.0,
//       max: 100.0,
//       activeColor: Colors.blue,
//       inactiveColor: Colors.grey,
//       label: '$_value',
//       divisions: 10,
//
//       // Customize the slider thumb shape
//       thumbShape: CustomSliderThumbCircle(
//         thumbRadius: 15.0,
//         thumbHeight: 30.0,
//       ),
//     );
//   }
// }
//
// void main() =>
//     runApp(MaterialApp(home: Scaffold(body: Center(child: CustomSlider()))));
