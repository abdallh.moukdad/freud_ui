import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:mood_tracking_freud_ui/core/router/app_route_constant.dart';
import 'package:mood_tracking_freud_ui/features/assessment/gender/presentation/view/gender_view.dart';
import 'package:mood_tracking_freud_ui/features/assessment/goal_for_today/presentation/view/today_goal_view.dart';
import 'package:mood_tracking_freud_ui/features/page_view/assessment_page_view.dart';

final router = GoRouter(
  initialLocation: MyAppRouteConstraint.pageViewRouteName,
  routes: <RouteBase>[
    GoRoute(
      path: MyAppRouteConstraint.pageViewRouteName,
      builder: (context, state) => Scaffold(body: PageViewExample()),
    ),
    // GoRoute(
    //   path: MyAppRouteConstraint.pageViewRouteName,
    //   pageBuilder: (context, state) => MaterialPage(child: PageViewExample()),
    // ),
    GoRoute(
      path: MyAppRouteConstraint.goalForTodayRouteName,
      builder: (context, state) => GoalForToday(),
    ),
    GoRoute(
      path: MyAppRouteConstraint.genderViewRouteName,
      builder: (context, state) => GenderView(),
    ),
  ],
);
