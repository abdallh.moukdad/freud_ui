import 'package:flutter/material.dart';

//---------------Styles-----------------------
const kPrimaryColor = Color(0xff100B20);
const kTranstionDuration = Duration(milliseconds: 250);
const kGtSectraFine = 'GT Sectra Fine';

//------------------Assets--------------------
const KBackButtonPath = 'assets/svg/back_button.svg';
const KSolidHeartPath = 'assets/svg/Solid_heart.svg';
const KflagPath = 'assets/svg/flag.svg';
const KmousePath = 'assets/svg/mouse.svg';
const KSmiledFacePath = 'assets/svg/smiled_face.svg';
const KHeadEmojiPath = 'assets/svg/head_emoji.svg';
const KRoundPath = 'assets/svg/round.svg';
const KArrowRightPath = 'assets/svg/Monotone_arrow_right_sm.svg';

const KManSymbolPath = 'assets/svg/Solid_arrow_right_sm_male.svg';
const KWomenSymbolPath = 'assets/svg/Solid_arrow_right_sm_female.svg';
const KManPath = 'assets/svg/Man.svg';
const KWomenPath = 'assets/svg/Women.svg';
const KXIconPath = 'assets/svg/X_icon.svg';
const KEmotionNeutral = 'assets/svg/Emotion_neutral.svg';
const KTwoArrow = 'assets/svg/two_arrow.svg';

const KSoughtHelp = 'assets/svg/sought_help.svg';
const KWhiteX= 'assets/svg/whiteX_icon.svg';
const KBrownTrueMark= 'assets/svg/brown_true_mark.svg';
const KYellwoShape= 'assets/svg/yellwo_shape.svg';
const KSadFace= 'assets/svg/sad_face.svg';
const KNFace= 'assets/svg/n_face.svg';
const KHappyFace= 'assets/svg/happy_face.svg';
