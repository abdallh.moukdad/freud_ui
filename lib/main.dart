import 'package:flutter/material.dart';

import 'core/router/router.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      // color: Colors.white,
      routerConfig: router,
      title: 'Flutter Demo',
      theme: ThemeData(
        // useMaterial3: false,
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple)

      ),
    );
  }
}
