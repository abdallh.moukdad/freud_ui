import 'package:flutter/material.dart';
import 'package:mood_tracking_freud_ui/core/widgets/goal_app_bar.dart';
import 'package:mood_tracking_freud_ui/core/widgets/primary_button.dart';
import 'package:numberpicker/numberpicker.dart';

class AgeView extends StatelessWidget {
  const AgeView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      // child: MyAppBar(),
      child: SingleChildScrollView(
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Center(
              child: Container(
                constraints: BoxConstraints(maxWidth: 400),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GoalAppBar(
                    pageNumber: '3',
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            // GoalBody(),
            AgeBody(),
          ],
        ),
      ),
    );
  }
}

class AgeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AreYouTakingAnyMedicationsOrSupplements(),
        // NumberPickerDemo (),
        SizedBox(
          height: 20,
        ),
        SizedBox(width: 256, height: 410, child: _IntegerExample()),

        ContinueButton()
      ],
    );
  }
}

class _IntegerExample extends StatefulWidget {
  @override
  __IntegerExampleState createState() => __IntegerExampleState();
}

class __IntegerExampleState extends State<_IntegerExample> {
  int _currentValue = 3;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        NumberPicker(
          // infiniteLoop: true,
          // zeroPad: true,
          itemHeight: 130,
          itemWidth: 256,
          decoration: BoxDecoration(
            color: Color(0xFF9BB067),
            borderRadius: BorderRadius.circular(1234),
            border: Border(
              left: BorderSide(color: Color(0xFFF2F4EA)),
              top: BorderSide(width: 1, color: Color(0xFFF2F4EA)),
              right: BorderSide(color: Color(0xFFF2F4EA)),
              bottom: BorderSide(width: 1, color: Color(0xFFF2F4EA)),
            ),
            boxShadow: [
              BoxShadow(
                color: Color(0x3F9BB068),
                blurRadius: 0,
                offset: Offset(0, 0),
                spreadRadius: 4,
              )
            ],
          ),
          textStyle: TextStyle(
            color: Color(0xFFACA8A5),
            fontSize: 60,
            fontFamily: 'Urbanist',
            fontWeight: FontWeight.w800,
            height: 0.02,
            letterSpacing: -1.08,
          ),
          selectedTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 128,
            fontFamily: 'Urbanist',
            fontWeight: FontWeight.w800,
            height: 0.01,
            letterSpacing: -3.20,
          ),
          value: _currentValue,
          minValue: 0,
          maxValue: 100,
          onChanged: (value) => setState(() => _currentValue = value),
        ),
        // Text('Current value: $_currentValue'),
      ],
    );
  }
}

class AreYouTakingAnyMedicationsOrSupplements extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 343,
      child: Text(
        'What’s your age?',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Color(0xFF4E3321),
          fontSize: 30,
          fontFamily: 'Urbanist',
          fontWeight: FontWeight.w800,
          height: 0.04,
          letterSpacing: -0.30,
        ),
      ),
    );
  }
}
