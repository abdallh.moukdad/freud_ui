import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mood_tracking_freud_ui/core/constants/constants.dart';
import 'package:mood_tracking_freud_ui/core/widgets/goal_app_bar.dart';
import 'package:mood_tracking_freud_ui/core/widgets/primary_button.dart';

class PhysicalDistressView extends StatelessWidget {
  const PhysicalDistressView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                constraints: BoxConstraints(maxWidth: 400),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GoalAppBar(
                    pageNumber: 7,
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              PhysicalDistressViewBody(),
            ],
          ),
        ),
      ),
    );
  }
}

class PhysicalDistressViewBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AreYouExperiencingAnyPhysicalSymptomsOfDistress(),
        SizedBox(
          height: 48,
        ),
        // Frame(),
        AssessmentDistressLevel(
          text: 'Yes, one or multiple',
          subText:
              'I’m experiencing physical pain in different place over my body.',
          iconPath: KBrownTrueMark,
        ),
        SizedBox(
          height: 12,
        ),
        AssessmentDistressLevel(
          text: 'No Physical Pain At All',
          subText:
              'I’m not experiencing any physical pain in my body at all :)',
          iconPath: KWhiteX,
        ),
        SizedBox(
          height: 48,
        ),
        ContinueButton(),
      ],
    );
  }
}

class AreYouExperiencingAnyPhysicalSymptomsOfDistress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: 343,
          child: Text(
            'Are you experiencing any physical distress?',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Color(0xFF4E3321),
              fontSize: 30,
              fontFamily: 'Urbanist',
              fontWeight: FontWeight.w800,
              // height: 0.04,
              letterSpacing: -0.30,
            ),
          ),
        ),
      ],
    );
  }
}

class AssessmentDistressLevel extends StatefulWidget {
  final text;
  final subText;
  final iconPath;

  const AssessmentDistressLevel(
      {super.key, this.text, this.subText, this.iconPath});

  @override
  State<AssessmentDistressLevel> createState() =>
      _AssessmentDistressLevelState();
}

class _AssessmentDistressLevelState extends State<AssessmentDistressLevel> {
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              isSelected = !isSelected;
            });
          },
          child: Container(
            width: 343,
            height: 174,
            padding: const EdgeInsets.all(16),
            clipBehavior: Clip.antiAlias,
            decoration: ShapeDecoration(
              color: isSelected == true ? Color(0xFF9BB067) : Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32),
              ),
              shadows: isSelected == true
                  ? [
                      BoxShadow(
                        color: Color(0x3F9BB068),
                        blurRadius: 0,
                        offset: Offset(0, 0),
                        spreadRadius: 4,
                      )
                    ]
                  : [
                      BoxShadow(
                        color: Color(0x0C4B3425),
                        blurRadius: 16,
                        offset: Offset(0, 8),
                        spreadRadius: 0,
                      )
                    ],
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 48,
                          height: 48,
                          padding: const EdgeInsets.all(12),
                          clipBehavior: Clip.antiAlias,
                          decoration: ShapeDecoration(
                            color: isSelected
                                ? Color(0xFFB4C38C)
                                : Color(0xFFF7F4F2),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(1234),
                            ),
                          ),
                          child: SvgPicture.asset(
                            widget.iconPath,
                            theme: SvgTheme(
                                currentColor:
                                    isSelected ? Colors.white : Colors.green),
                            color: isSelected ? Colors.white : Colors.brown,
                          ),
                        ),
                        const SizedBox(height: 16),
                        Container(
                          width: double.infinity,
                          height: 78,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget.text,
                                style: TextStyle(
                                  color: isSelected == true
                                      ? Colors.white
                                      : Color(0xFF4E3321),
                                  fontSize: 18,
                                  fontFamily: 'Urbanist',
                                  fontWeight: FontWeight.w800,
                                  // height: 0,
                                  letterSpacing: -0.07,
                                ),
                              ),
                              const SizedBox(height: 4),
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  widget.subText,
                                  style: TextStyle(
                                    color: isSelected == true
                                        ? Colors.white
                                        : Color(0xFF736A66),
                                    fontSize: 16,
                                    fontFamily: 'Urbanist',
                                    fontWeight: FontWeight.w500,
                                    // height: 0.10,
                                    letterSpacing: -0.05,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: 24,
                  height: 24,
                  child: Stack(
                    children: [
                      Positioned(
                        left: 3,
                        top: 3,
                        child: Container(
                          width: 18,
                          height: 18,
                          decoration: ShapeDecoration(
                            shape: OvalBorder(
                              side: BorderSide(
                                width: 2,
                                strokeAlign: BorderSide.strokeAlignCenter,
                                color: isSelected ? Colors.white : Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ),
                      isSelected == true
                          ? Positioned(
                              left: 7,
                              top: 7,
                              child: Container(
                                width: 10,
                                height: 10,
                                decoration: ShapeDecoration(
                                  color: Colors.white,
                                  shape: OvalBorder(),
                                ),
                              ),
                            )
                          : Container()
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class Frame extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 343,
          height: 174,
          padding: const EdgeInsets.all(16),
          clipBehavior: Clip.antiAlias,
          decoration: ShapeDecoration(
            color: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(32),
            ),
            shadows: [
              BoxShadow(
                color: Color(0x0C4B3425),
                blurRadius: 16,
                offset: Offset(0, 8),
                spreadRadius: 0,
              )
            ],
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          width: 48,
                          height: 48,
                          padding: const EdgeInsets.all(12),
                          clipBehavior: Clip.antiAlias,
                          decoration: ShapeDecoration(
                            color: Color(0xFFF7F4F2),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(1234),
                            ),
                          ),
                          child: SvgPicture.asset(
                            KBrownTrueMark,
                            fit: BoxFit.contain,
                          )),
                      const SizedBox(height: 16),
                      Container(
                        width: double.infinity,
                        height: 78,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Yes, one or multiple',
                              style: TextStyle(
                                color: Color(0xFF4E3321),
                                fontSize: 18,
                                fontFamily: 'Urbanist',
                                fontWeight: FontWeight.w800,
                                // height: 0,
                                letterSpacing: -0.07,
                              ),
                            ),
                            const SizedBox(height: 4),
                            SizedBox(
                              width: double.infinity,
                              child: Text(
                                'I’m experiencing physical pain in different place over my body.',
                                style: TextStyle(
                                  color: Color(0xFF736A66),
                                  fontSize: 16,
                                  fontFamily: 'Urbanist',
                                  fontWeight: FontWeight.w500,
                                  // height: 0.10,
                                  letterSpacing: -0.05,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              // Container(
              //   width: 24,
              //   height: 24,
              //   padding: const EdgeInsets.all(3),
              //   child: Row(
              //     mainAxisSize: MainAxisSize.min,
              //     mainAxisAlignment: MainAxisAlignment.center,
              //     crossAxisAlignment: CrossAxisAlignment.center,
              //     children: [
              //       Container(
              //         width: 18,
              //         height: 18,
              //         decoration: ShapeDecoration(
              //           shape: OvalBorder(
              //             side: BorderSide(
              //               width: 2,
              //               strokeAlign: BorderSide.strokeAlignCenter,
              //               color: Color(0xFF4E3321),
              //             ),
              //           ),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
            ],
          ),
        ),
      ],
    );
  }
}
