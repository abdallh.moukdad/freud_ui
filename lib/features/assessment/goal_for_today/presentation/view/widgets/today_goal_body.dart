import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mood_tracking_freud_ui/core/constants/constants.dart';
import 'package:mood_tracking_freud_ui/core/widgets/assessment_text_widget.dart';
import 'package:mood_tracking_freud_ui/core/widgets/primary_button.dart';

class GoalBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
      // Text(
      //     "What's your health goal for today?",
      //     textAlign: TextAlign.center,
      //     // overflow: TextOverflow.fade,
      //     style: TextStyle(
      //       color: Color(0xFF4E3321),
      //       fontSize: 30,
      //       fontFamily: 'Urbanist',
      //       fontWeight: FontWeight.w800,
      //       // height: 0.04,
      //       letterSpacing: -0.30,
      //     ),
      //   ),
        AssessmentText(text:  "What's your health goal for today?",),
        SizedBox(
          height: 38,
        ),
        GoalsContainer(
          colour: Colors.white,
          text: "I wanna reduce stress",
          iconPath: KSolidHeartPath,
        ),
        SizedBox(
          height: 12,
        ),
        GoalsContainer(
          colour: Colors.white,
          text: "I wanna try AI Therapy",
          iconPath: KHeadEmojiPath,
        ),
        SizedBox(
          height: 12,
        ),
        GoalsContainer(
          colour: Colors.white,
          text: "I want to cope with trauma",
          iconPath: KflagPath,
        ),
        SizedBox(
          height: 12,
        ),
        GoalsContainer(
          colour: Colors.white,
          text: "I want to be a better person",
          iconPath: KSmiledFacePath,
        ),
        SizedBox(
          height: 12,
        ),
        GoalsContainer(
          colour: Colors.white,
          text: "Just trying out the app, mate!",
          iconPath: KmousePath,
        ),
        SizedBox(
          height: 40,
        ),
        // ButtonPrimaryIcon(),
        ContinueButton(),
        // RadioExample(),
      ],
    );
  }
}

class GoalsContainer extends StatefulWidget {
  Color colour = Colors.white;
  final iconPath;
  final String text;

  GoalsContainer(
      {super.key, required this.colour, this.iconPath, required this.text});

  @override
  State<GoalsContainer> createState() => _GoalsContainerState();
}

class _GoalsContainerState extends State<GoalsContainer> {
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          // isSelected = !isSelected;
          isSelected ==true ? isSelected=false: isSelected=true;
          isSelected? widget.colour = Color(0xFF9BB067): widget.colour = Colors.white;
              // ? widget.colour = Colors.white
              // : widget.colour = Color(0xFF9BB067);

          // widget.colour=Colors.green;
        });
      },
      child: Container(
        width: 343,
        height: 56,
        padding: const EdgeInsets.all(16),
        decoration: ShapeDecoration(
          color: widget.colour,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(1234),
          ),
          shadows: [
            BoxShadow(
              color: Color(0x0C4B3425),
              blurRadius: 16,
              offset: Offset(0, 8),
              spreadRadius: 0,
            )
          ],
        ),
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            // SolidHeart(),
            SvgIcon(
              iconPath: widget.iconPath,
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              '${widget.text}',
              style: TextStyle(
                color: Color(0xFF4E3321),
                fontSize: 16,
                fontFamily: 'Urbanist',
                fontWeight: FontWeight.w700,
                height: 0,
                letterSpacing: -0.05,
              ),
            ),
            // SizedBox(
            //   width: 120,
            // ),
            Spacer(),
            Container(
              width: 18,
              height: 18,
              decoration: ShapeDecoration(
                shape: OvalBorder(
                  side: BorderSide(
                    width: 2,
                    strokeAlign: BorderSide.strokeAlignCenter,
                    color: Color(0xFF4E3321),
                  ),
                ),
              ),
              child: isSelected ? SvgPicture.asset(KRoundPath) :null,
            ),
          ],
        ),
      ),
    );
  }
}

class SvgIcon extends StatelessWidget {
  final iconPath;

  const SvgIcon({super.key, this.iconPath});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            width: 24,
            height: 24,
            padding: const EdgeInsets.only(
              top: 4.05,
              left: 2.05,
              right: 2.05,
              bottom: 2,
            ),
            child: SvgPicture.asset(
              iconPath,
              fit: BoxFit.cover,
              height: 30,
              width: 30,
            )),
      ],
    );
  }
}

class SolidHeart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            width: 24,
            height: 24,
            padding: const EdgeInsets.only(
              top: 4.05,
              left: 2.05,
              right: 2.05,
              bottom: 2,
            ),
            child: SvgPicture.asset(
              KSolidHeartPath,
              fit: BoxFit.cover,
              height: 30,
              width: 30,
            )),
      ],
    );
  }
}

class MonotoneCheckboxEmpty extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 24,
          height: 24,
          padding: const EdgeInsets.all(3),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 18,
                height: 18,
                decoration: ShapeDecoration(
                  shape: OvalBorder(
                    side: BorderSide(
                      width: 2,
                      strokeAlign: BorderSide.strokeAlignCenter,
                      color: Color(0xFF4E3321),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class Frame1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 343,
          height: 56,
          padding: const EdgeInsets.all(16),
          decoration: ShapeDecoration(
            color: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(1234),
            ),
            shadows: [
              BoxShadow(
                color: Color(0x0C4B3425),
                blurRadius: 16,
                offset: Offset(0, 8),
                spreadRadius: 0,
              )
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: double.infinity,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Container(
                        height: 24,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              width: 24,
                              height: 24,
                              padding: const EdgeInsets.only(
                                top: 4.05,
                                left: 2.05,
                                right: 2.05,
                                bottom: 2,
                              ),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [],
                              ),
                            ),
                            const SizedBox(width: 8),
                            Text(
                              'I wanna reduce stress',
                              style: TextStyle(
                                color: Color(0xFF4E3321),
                                fontSize: 16,
                                fontFamily: 'Urbanist',
                                fontWeight: FontWeight.w700,
                                height: 0,
                                letterSpacing: -0.05,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(width: 16),
                    Container(
                      width: 24,
                      height: 24,
                      padding: const EdgeInsets.all(3),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: 18,
                            height: 18,
                            decoration: ShapeDecoration(
                              shape: OvalBorder(
                                side: BorderSide(
                                  width: 2,
                                  strokeAlign: BorderSide.strokeAlignCenter,
                                  color: Color(0xFF4E3321),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
