import 'package:flutter/material.dart';
import 'package:mood_tracking_freud_ui/core/widgets/goal_app_bar.dart';
import 'package:mood_tracking_freud_ui/features/assessment/goal_for_today/presentation/view/widgets/today_goal_body.dart';

class GoalForToday extends StatelessWidget {
  const GoalForToday({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      // child: MyAppBar(),
      child: SingleChildScrollView(
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Center(
              child: Container(
                constraints: BoxConstraints(
                  maxWidth: 400
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GoalAppBar(),
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            GoalBody(),
          ],
        ),
      ),
    );
  }
}
