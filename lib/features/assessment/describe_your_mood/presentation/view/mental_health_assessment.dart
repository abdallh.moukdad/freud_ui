import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mood_tracking_freud_ui/core/widgets/goal_app_bar.dart';

import '../../../../../core/constants/constants.dart';

class MentalHealthAssessment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            width: 375,
            height: 812,
            clipBehavior: Clip.antiAlias,
            decoration: ShapeDecoration(
              color: Color(0xFFF7F4F2),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40),
              ),
            ),
            child: Stack(
              children: [
                Positioned(
                  child: GoalAppBar(
                    pageNumber: 5,
                  ),
                  left: 1,
                  top: 10,
                ),
                Positioned(
                  left: 43,
                  top: 148,
                  child: Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 290,
                          child: Text(
                            'How would you describe your mood?',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0xFF4E3321),
                              fontSize: 30,
                              fontFamily: 'Urbanist',
                              fontWeight: FontWeight.w800,
                              // height: 0.04,
                              letterSpacing: -0.30,
                            ),
                          ),
                        ),
                        const SizedBox(height: 48),
                        Container(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'I Feel Neutral.',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Color(0xFF736A66),
                                  fontSize: 20,
                                  fontFamily: 'Urbanist',
                                  fontWeight: FontWeight.w700,
                                  height: 0,
                                  letterSpacing: -0.10,
                                ),
                              ),
                              const SizedBox(height: 24),
                              SvgPicture.asset(KEmotionNeutral),
                              const SizedBox(height: 24),
                              SvgPicture.asset(KTwoArrow),
                              const SizedBox(height: 24),
                              // Container(
                              //   width: 48,
                              //   height: 48,
                              //   padding: const EdgeInsets.only(
                              //     top: 12.59,
                              //     left: 10.48,
                              //     right: 10.48,
                              //     bottom: 12,
                              //   ),
                              //   child: Row(
                              //     mainAxisSize: MainAxisSize.min,
                              //     mainAxisAlignment: MainAxisAlignment.center,
                              //     crossAxisAlignment: CrossAxisAlignment.center,
                              //     children: [],
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  left: -23,
                  top: 608,
                  child: Container(
                    width: 422,
                    height: 525,
                    child: Stack(
                      children: [
                        Positioned(
                          left: 0,
                          top: 103,
                          child: Container(
                            width: 422,
                            height: 422,
                            decoration: ShapeDecoration(
                              shape: OvalBorder(
                                side: BorderSide(
                                  width: 160,
                                  strokeAlign: BorderSide.strokeAlignOutside,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          left: 0,
                          top: 103,
                          child: Container(
                            width: 422,
                            height: 422,
                            decoration: ShapeDecoration(
                              shape: OvalBorder(
                                side: BorderSide(
                                  width: 160,
                                  strokeAlign: BorderSide.strokeAlignOutside,
                                  color: Color(0xFFFFCE5B),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          // left: 250,
                          left: 0,
                          top: 103,
                          child: Container(
                            width: 42,
                            height: 42,
                            decoration: ShapeDecoration(
                              shape: OvalBorder(
                                side: BorderSide(
                                  width: 160,
                                  strokeAlign: BorderSide.strokeAlignOutside,
                                  color: Color(0xFFEC7D1C),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          // left: 350,

                          left: 0,
                          top: 103,
                          child: Container(
                            width: 422,
                            height: 422,
                            decoration: ShapeDecoration(
                              shape: OvalBorder(
                                side: BorderSide(
                                  width: 160,
                                  strokeAlign: BorderSide.strokeAlignOutside,
                                  color: Color(0xFF9BB067),
                                ),
                              ),
                            ),
                          ),
                        ),
                        // Positioned(
                        //   left: 0,
                        //   top: 103,
                        //   child: Container(
                        //     width: 422,
                        //     height: 422,
                        //     decoration: ShapeDecoration(
                        //       shape: OvalBorder(
                        //         side: BorderSide(
                        //           width: 160,
                        //           strokeAlign: BorderSide.strokeAlignOutside,
                        //           color: Color(0xFFA694F5),
                        //         ),
                        //       ),
                        //     ),
                        //   ),
                        // ),
                        // Positioned(
                        //   left: 0,
                        //   top: 103,
                        //   child: Container(
                        //     width: 422,
                        //     height: 422,
                        //     decoration: ShapeDecoration(
                        //       shape: OvalBorder(
                        //         side: BorderSide(
                        //           width: 160,
                        //           strokeAlign: BorderSide.strokeAlignOutside,
                        //           color: Color(0xFFA80C0C),
                        //         ),
                        //       ),
                        //     ),
                        //   ),
                        // ),
                        Positioned(
                          left: 0,
                          top: 103,
                          child: Opacity(
                            opacity: 0.15,
                            child: Container(
                              width: 422,
                              height: 422,
                              decoration: ShapeDecoration(
                                shape: OvalBorder(
                                  side: BorderSide(
                                    width: 16,
                                    strokeAlign: BorderSide.strokeAlignOutside,
                                    color: Color(0xFF4E3321),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        // Positioned(
                        //   left: 229,
                        //   top: 134,
                        //   child: Transform(
                        //     transform: Matrix4.identity()
                        //       ..translate(0.0, 0.0)
                        //       ..rotateZ(-3.14),
                        //     child: Container(
                        //       width: 37,
                        //       height: 56,
                        //       child: Stack(
                        //         children: [
                        //           Positioned(
                        //             left: 24,
                        //             top: 22,
                        //             child: Transform(
                        //               transform: Matrix4.identity()
                        //                 ..translate(0.0, 0.0)
                        //                 ..rotateZ(3.14),
                        //               child: Container(
                        //                 width: 12,
                        //                 height: 12,
                        //                 clipBehavior: Clip.antiAlias,
                        //                 decoration: ShapeDecoration(
                        //                   color: Colors.white,
                        //                   shape: RoundedRectangleBorder(
                        //                     borderRadius:
                        //                         BorderRadius.circular(1234),
                        //                   ),
                        //                 ),
                        //               ),
                        //             ),
                        //           ),
                        //         ],
                        //       ),
                        //     ),
                        //   ),
                        // ),
                        // Positioned(
                        //   left: 144,
                        //
                        //   // top: -60,
                        //   bottom: 440,
                        //   child: Container(
                        //     width: 152.50,
                        //     height: 137.50,
                        //
                        //     child: SvgPicture.asset(KYellwoShape,color: Colors.red,),
                        //
                        //   ),
                        // ),
                        // Positioned(
                        //   left: 295.50,
                        //   // top: 25,
                        //   // bottom: 420,
                        //   child: Transform(
                        //     transform: Matrix4.identity()
                        //       ..translate(0.0, 0.0)
                        //       ..rotateZ(0.52),
                        //     child: Container(
                        //       width: 160,
                        //       height: 145,
                        //       child: SvgPicture.asset(KYellwoShape,
                        //       color: Colors.green,),
                        //     ),
                        //   ),
                        // ),
                        // Positioned(
                        //   left: 12,
                        //   top: 101,
                        //   child: Transform(
                        //     transform: Matrix4.identity()
                        //       ..translate(0.0, 0.0)
                        //       ..rotateZ(-0.52),
                        //     child: Container(
                        //       width: 152.50,
                        //       height: 143.90,
                        //       child: SvgPicture.asset(KYellwoShape),
                        //     ),
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  left: 89,

                  // top: -60,
                  bottom: 98,
                  child: Container(
                    width: 182.50,
                    height: 173.90,
                    child: Stack(children: [
                      SvgPicture.asset(
                        KYellwoShape,
                        // color: Colors.red,
                      ),
                      Positioned(
                          top: 56, left: 65, child: SvgPicture.asset(KNFace))
                    ]),
                  ),
                ),
                Positioned(
                  left: 278.50,
                  // top: 25,
                  bottom: 99,
                  child: Transform(
                    transform: Matrix4.identity()
                      ..translate(0.0, 0.0)
                      ..rotateZ(0.52),
                    child: Container(
                      width: 182.50,
                      height: 173.90,
                      child: Stack(
                        children: [
                          SvgPicture.asset(
                            KYellwoShape,
                            color: Color(0xFF9BB168),
                          ),
                          Positioned(
                              top: 50,
                              left: 50,
                              child: SvgPicture.asset(KHappyFace))
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: -68,
                  // top: 101,
                  bottom: 5,
                  child: Transform(
                    transform: Matrix4.identity()
                      ..translate(0.0, 0.0)
                      ..rotateZ(-0.52),
                    child: Container(
                      width: 182.50,
                      height: 173.90,
                      child: Stack(children: [
                        SvgPicture.asset(
                          KYellwoShape,
                          color: Color(0xFFED7E1C),
                        ),
                        Positioned(
                            left: 80,
                            top: 40,
                            child: Transform(
                                transform: Matrix4.identity()
                                  ..translate(0.0, 0.0)
                                  ..rotateZ(0.52),
                                child: SvgPicture.asset(KSadFace))),
                      ]),
                    ),
                  ),
                ),
                //newwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
                // Positioned(
                //   left: 229,
                //   top: 134,
                //   child: Transform(
                //     transform: Matrix4.identity()..translate(0.0, 0.0)..rotateZ(-3.14),
                //     child: Container(
                //       width: 37,
                //       height: 56,
                //       child: Stack(
                //         children: [
                //           Positioned(
                //             left: 24,
                //             top: 22,
                //             child: Transform(
                //               transform: Matrix4.identity()..translate(0.0, 0.0)..rotateZ(3.14),
                //               child: Container(
                //                 width: 12,
                //                 height: 12,
                //                 clipBehavior: Clip.antiAlias,
                //                 decoration: ShapeDecoration(
                //                   color: Colors.white,
                //                   shape: RoundedRectangleBorder(
                //                     borderRadius: BorderRadius.circular(1234),
                //                   ),
                //                 ),
                //               ),
                //             ),
                //           ),
                //         ],
                //       ),
                //     ),
                //   ),
                // ),
                // Positioned(
                //   left: 280,
                //   top: 550,
                //   child: Transform(
                //     transform: Matrix4.identity()..rotateZ(0.45),
                //     child: Container(
                //       color: Color(0xFF9BB067),
                //       width: 152.50,
                //       height: 137.50,
                //       child: Stack(children: [
                //
                //           ]),
                //     ),
                //   ),
                // ),
                // Positioned(
                //   // left: 339.50,
                //   left: -60,
                //   top: 630,
                //   child: Transform(
                //     transform: Matrix4.identity()..translate(0.0, 0.0)..rotateZ(-0.45),
                //     child: Container(
                //       color: Color(0xFFEC7D1C),
                //       width: 160,
                //       height: 145,
                //       child: Stack(children: [
                //
                //           ]),
                //     ),
                //   ),
                // ),
                // Positioned(
                //   left: 100,
                //   top: 551,
                //   child: Transform(
                //     transform: Matrix4.identity()..translate(0.0, 0.0)//..rotateZ(-0.45),
                //     ,child: ClipPath(
                //     clipper: TrapezoidClipper(),
                //       child: Container(
                //         color:Color(0xFFFFCE5B),
                //         width: 152.50,
                //         height: 143.90,
                //         child: Stack(children: [
                //
                //             ]),
                //       ),
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class TrapezoidClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();

    path.lineTo(0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width * 0.3, 0);
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
