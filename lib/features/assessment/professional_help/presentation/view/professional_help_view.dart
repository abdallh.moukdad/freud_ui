import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mood_tracking_freud_ui/core/constants/constants.dart';
import 'package:mood_tracking_freud_ui/core/widgets/goal_app_bar.dart';
import 'package:mood_tracking_freud_ui/core/widgets/primary_button.dart';

class ProfessionalHelpView extends StatelessWidget {
  const ProfessionalHelpView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                constraints: BoxConstraints(maxWidth: 400),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GoalAppBar(
                    pageNumber: 6,
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              ProfessionalHelpBody(),
            ],
          ),
        ),
      ),
    );
  }
}

class ProfessionalHelpBody extends StatelessWidget {
  const ProfessionalHelpBody({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        HaveYouSoughtProfessionalHelpBefore(),
        SizedBox(
          height: 40,
        ),
        HaveYouSoughtProfessionalHelpBeforeImage(),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            YesNOButton(
              text: 'Yes',
              buttonColour: Color(0xFF9BB067),
              textColour: Colors.white,
            ),
            SizedBox(
              width: 16,
            ),
            YesNOButton(
              text: 'NO',
              buttonColour: Colors.white,
              textColour: Colors.black,
            ),
          ],
        ),
        SizedBox(height: 16),
        ContinueButton()
      ],
    );
  }
}

class HaveYouSoughtProfessionalHelpBefore extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 450,
      child: Text(
        'Have you sought professional help before?',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Color(0xFF4E3321),
          fontSize: 30,
          fontFamily: 'Urbanist',
          fontWeight: FontWeight.w800,
          // height: 0.04,
          letterSpacing: -0.30,
        ),
      ),
    );
  }
}

class HaveYouSoughtProfessionalHelpBeforeImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 286,
          height: 286,
          padding: const EdgeInsets.only(
            top: 3,
            left: 14,
            right: 13.01,
            bottom: 3.88,
          ),
          decoration: ShapeDecoration(
            color: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(1234),
            ),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 258.99,
                height: 279.12,
                child: SvgPicture.asset(KSoughtHelp),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class YesNOButton extends StatelessWidget {
  const YesNOButton({super.key, this.text, this.buttonColour, this.textColour});

  final text;
  final textColour;
  final buttonColour;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      clipBehavior: Clip.antiAlias,
      style: ButtonStyle(
          shadowColor: MaterialStateProperty.all(Color(0x3F9BB068)),
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(1000),
            ),
          ),
          backgroundColor: MaterialStateProperty.all(
            buttonColour,
          ),
          minimumSize: MaterialStateProperty.all(Size(164, 56))),
      onPressed: () {},
      child: Text(
        text,
        style: TextStyle(
          color: textColour,
          fontSize: 18,
          fontFamily: 'Urbanist',
          fontWeight: FontWeight.w800,
          height: 0,
          letterSpacing: -0.07,
        ),
      ),
    );
  }
}
