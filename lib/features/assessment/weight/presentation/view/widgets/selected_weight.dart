import 'package:flutter/material.dart';

class SelectedWeight extends StatelessWidget {
  const SelectedWeight({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 12,
      height: 118,
      decoration: ShapeDecoration(
        color: const Color(0xFF9BB067),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(1234),
        ),
        shadows: const [
          BoxShadow(
            color: Color(0x3F9BB068),
            blurRadius: 0,
            offset: Offset(0, 0),
            spreadRadius: 4,
          )
        ],
      ),
    );
  }
}
