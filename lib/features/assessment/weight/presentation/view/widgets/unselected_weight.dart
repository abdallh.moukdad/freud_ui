import 'package:flutter/material.dart';

class UnSelectedWeight extends StatelessWidget {
  const UnSelectedWeight({super.key});

  @override
  Widget build(BuildContext context) {
    return  Container(
      height: 24,
      clipBehavior: Clip.antiAlias,
      decoration: const ShapeDecoration(
        shape: RoundedRectangleBorder(
          side: BorderSide(
            width: 4,
            strokeAlign: BorderSide.strokeAlignCenter,
            color: Color(0xFFE8DCD8),
          ),
          borderRadius: BorderRadius.all(Radius.circular(1234)),
        ),
      ),
    );
  }
}
