import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mood_tracking_freud_ui/features/assessment/weight/presentation/manger/screen_cubit/screen_cubit.dart';

class KgLbs extends StatefulWidget {
  const KgLbs({super.key, this.text, this.defaultSelected = false});

  final bool defaultSelected;
  final text;

  @override
  State<KgLbs> createState() => _KgLbsState();
}

class _KgLbsState extends State<KgLbs> {
  @override
  void initState() {
    // TODO: implement initState
    if (widget.defaultSelected) {
      selected = widget.defaultSelected;
    }
    super.initState();
  }

  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ScreenCubit, ScreenState>(
      builder: (context, state) {
        selected = context.read<ScreenCubit>().weightMeasure == widget.text
            ? true
            : false;
        if (state is ChangeWeightMeasure)
          return Expanded(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  context.read<ScreenCubit>().changeWeightMeasure(widget.text);
                  selected = !selected;
                });
              },
              child: Container(
                height: 48,
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(
                  color: selected ? Color(0xFFEC7D1C) : Colors.white,
                  borderRadius: BorderRadius.circular(1234),
                  boxShadow: selected
                      ? [
                          BoxShadow(
                            color: Color(0x3FFE814B),
                            blurRadius: 0,
                            offset: Offset(0, 0),
                            spreadRadius: 4,
                          )
                        ]
                      : [],
                ),
                child: Center(
                  child: Text(
                    widget.text,
                    style: TextStyle(
                      color: selected ? Colors.white : Color(0xFF4E3321),
                      fontSize: 16,
                      fontFamily: 'Urbanist',
                      fontWeight: FontWeight.w800,
                      height: 0,
                      letterSpacing: -0.05,
                    ),
                  ),
                ),
              ),
            ),
          );
        else {
          return Expanded(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  context.read<ScreenCubit>().changeWeightMeasure(widget.text);
                  selected = !selected;
                });
              },
              child: Container(
                height: 48,
                padding:
                const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(
                  color: selected ? Color(0xFFEC7D1C) : Colors.white,
                  borderRadius: BorderRadius.circular(1234),
                  boxShadow: selected
                      ? [
                    BoxShadow(
                      color: Color(0x3FFE814B),
                      blurRadius: 0,
                      offset: Offset(0, 0),
                      spreadRadius: 4,
                    )
                  ]
                      : [],
                ),
                child: Center(
                  child: Text(
                    widget.text,
                    style: TextStyle(
                      color: selected ? Colors.white : Color(0xFF4E3321),
                      fontSize: 16,
                      fontFamily: 'Urbanist',
                      fontWeight: FontWeight.w800,
                      height: 0,
                      letterSpacing: -0.05,
                    ),
                  ),
                ),
              ),
            ),
          );
        }
      },
    );
  }
}
