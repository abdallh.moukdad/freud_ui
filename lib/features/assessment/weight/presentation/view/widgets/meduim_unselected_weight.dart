import 'package:flutter/material.dart';

class MeduimUnSelected extends StatelessWidget {
  const MeduimUnSelected({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 42,
      clipBehavior: Clip.antiAlias,
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          side: BorderSide(
            width: 8,
            strokeAlign: BorderSide
                .strokeAlignCenter,
            color: Color(0xFFD5C1B8),
          ),
        ),
      ),
    );
  }
}
