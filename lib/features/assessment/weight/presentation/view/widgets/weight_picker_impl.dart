import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mood_tracking_freud_ui/features/assessment/weight/presentation/manger/screen_cubit/screen_cubit.dart';
import 'package:mood_tracking_freud_ui/features/assessment/weight/presentation/view/widgets/weight_picker.dart';

class WeightPickerImpl extends StatefulWidget {
  const WeightPickerImpl({super.key});

  @override
  State<WeightPickerImpl> createState() => _WeightPickerImplState();
}

class _WeightPickerImplState extends State<WeightPickerImpl> {
  int _currentValue = 50;

  @override
  Widget build(BuildContext context) {
    return WeightPicker(
      textStyle: TextStyle(
        color: Color(0xFFD5C1B8),
        fontSize: 16,
        fontFamily: 'Urbanist',
        fontWeight: FontWeight.w600,
        height: 0,
        letterSpacing: -0.05,
      ),
      selectedTextStyle: TextStyle(
        color: Color(0xFF9F1515),
        fontSize: 16,
        fontFamily: 'Urbanist',
        fontWeight: FontWeight.w600,
        height: 0,
        letterSpacing: -0.05,
      ),
      minValue: 0,
      maxValue: 100,
      step: 1,
      itemCount: 5,
      value: context.read<ScreenCubit>().weightNumber,
      // itemCount: 400,
      axis: Axis.horizontal,
      onChanged: (int value) {
        setState(() {
          _currentValue = value;
          context.read<ScreenCubit>().changeWeight(value);
        });
      },
      itemHeight: 200,
      itemWidth: 100,
      zeroPad: false,
      infiniteLoop: false,
      haptics: false,
    );
  }
}
