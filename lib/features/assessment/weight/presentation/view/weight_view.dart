import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mood_tracking_freud_ui/core/widgets/goal_app_bar.dart';
import 'package:mood_tracking_freud_ui/core/widgets/primary_button.dart';
import 'package:mood_tracking_freud_ui/features/assessment/weight/presentation/manger/screen_cubit/screen_cubit.dart';

import 'widgets/kg_lbs_widget.dart';
import 'widgets/weight_picker_impl.dart';

class WeightView extends StatelessWidget {
  const WeightView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      // child: MyAppBar(),
      child: SingleChildScrollView(
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Center(
              child: Container(
                constraints: BoxConstraints(maxWidth: 400),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GoalAppBar(
                    pageNumber: '4',
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            // GoalBody(),
            WeightViewBody(),
          ],
        ),
      ),
    );
  }
}

class WeightViewBody extends StatelessWidget {
  int currentValue = 128;
  String currentMeasure='kg';
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Column(
          children: [
            // SizedBox(height: 100,),
            Container(
              width: 343,
              // height: 564,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: Text(
                      'What’s your weight?',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Color(0xFF4E3321),
                        fontSize: 30,
                        fontFamily: 'Urbanist',
                        fontWeight: FontWeight.w800,
                        height: 0.04,
                        letterSpacing: -0.30,
                      ),
                    ),
                  ),
                  const SizedBox(height: 48),
                  BlocProvider(
                    create: (context) => ScreenCubit(),
                    child: Container(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: double.infinity,
                            padding: const EdgeInsets.all(4),
                            decoration: ShapeDecoration(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32),
                              ),
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                KgLbs(
                                  text: 'kg',
                                  defaultSelected: true,
                                ),
                                KgLbs(text: 'lbs'),
                              ],
                            ),
                          ),
                          const SizedBox(height: 54),
                          Container(
                            child: SingleChildScrollView(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        BlocBuilder<ScreenCubit, ScreenState>(
                                          builder: (context, state) {
                                            if (state is ChangeWeightNumber) {
                                              currentValue = state.weightNumber;
                                              return Text(
                                                state.weightNumber.toString(),
                                                style: TextStyle(
                                                  color: Color(0xFF4E3321),
                                                  fontSize: 96,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.w800,
                                                  height: 0.01,
                                                  // letterSpacing: -1.92,
                                                ),
                                              );
                                            } else if (state is ScreenInitial) {
                                              currentValue =
                                                  state.initialWeightNumber;
                                              return Text(
                                                state.initialWeightNumber
                                                    .toString(),
                                                style: TextStyle(
                                                  color: Color(0xFF4E3321),
                                                  fontSize: 96,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.w800,
                                                  height: 0.01,
                                                  // letterSpacing: -1.92,
                                                ),
                                              );
                                            } else {
                                              return Text(
                                                currentValue.toString(),
                                                style: TextStyle(
                                                  color: Color(0xFF4E3321),
                                                  fontSize: 96,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.w800,
                                                  height: 0.01,
                                                  // letterSpacing: -1.92,
                                                ),
                                              );
                                            }
                                          },
                                        ),
                                        SizedBox(
                                          width: 50,
                                          height: 49,
                                          child: BlocBuilder<ScreenCubit,
                                              ScreenState>(
                                            builder: (context, state) {
                                              if (state
                                                  is ChangeWeightMeasure) {
                                                currentMeasure=state.weightMeasure;
                                                return Text(
                                                  state.weightMeasure,
                                                  style: TextStyle(
                                                    color: Color(0xFF736A66),
                                                    fontSize: 36,
                                                    fontFamily: 'Urbanist',
                                                    fontWeight: FontWeight.w600,
                                                    // height: 0.03,
                                                    letterSpacing: -0.43,
                                                  ),
                                                );
                                              }

                                              else if(state is ScreenInitial){
                                                currentMeasure=state.initialWeightMeasure;
                                                return Text(
                                                  state.initialWeightMeasure,
                                                  style: TextStyle(
                                                    color: Color(0xFF736A66),
                                                    fontSize: 36,
                                                    fontFamily: 'Urbanist',
                                                    fontWeight: FontWeight.w600,
                                                    // height: 0.03,
                                                    letterSpacing: -0.43,
                                                  ),
                                                );
                                              }
                                              else {
                                                return Text(
                                                  currentMeasure,
                                                  style: TextStyle(
                                                    color: Color(0xFF736A66),
                                                    fontSize: 36,
                                                    fontFamily: 'Urbanist',
                                                    fontWeight: FontWeight.w600,
                                                    // height: 0.03,
                                                    letterSpacing: -0.43,
                                                  ),
                                                );
                                              }
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(height: 24),
                                  WeightPickerImpl(),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(height: 4),
                          ContinueButton(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FrameGGG extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 343,
          height: 238,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      '128',
                      style: TextStyle(
                        color: Color(0xFF4E3321),
                        fontSize: 96,
                        fontFamily: 'Urbanist',
                        fontWeight: FontWeight.w800,
                        height: 0.01,
                        letterSpacing: -1.92,
                      ),
                    ),
                    SizedBox(
                      width: 39,
                      height: 49,
                      child: Text(
                        'kg',
                        style: TextStyle(
                          color: Color(0xFF736A66),
                          fontSize: 36,
                          fontFamily: 'Urbanist',
                          fontWeight: FontWeight.w600,
                          height: 0.03,
                          letterSpacing: -0.43,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 24),
              Container(
                width: 343,
                height: 118,
                child: Stack(
                  children: [
                    Positioned(
                      left: 0,
                      top: 0,
                      child: Container(
                        width: 343,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 42,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 8,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFD5C1B8),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 42,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 8,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFD5C1B8),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              width: 12,
                              height: 118,
                              decoration: ShapeDecoration(
                                color: Color(0xFF9BB067),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                                shadows: [
                                  BoxShadow(
                                    color: Color(0x3F9BB068),
                                    blurRadius: 0,
                                    offset: Offset(0, 0),
                                    spreadRadius: 4,
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 42,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 8,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFD5C1B8),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 42,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 8,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFD5C1B8),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Container(
                              height: 24,
                              clipBehavior: Clip.antiAlias,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    width: 4,
                                    strokeAlign: BorderSide.strokeAlignCenter,
                                    color: Color(0xFFE8DCD8),
                                  ),
                                  borderRadius: BorderRadius.circular(1234),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      left: 0,
                      top: 99,
                      child: Container(
                        width: 343,
                        height: 19,
                        child: Stack(
                          children: [
                            Positioned(
                              left: 246,
                              top: 0,
                              child: Text(
                                '128',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Color(0xFFD5C1B8),
                                  fontSize: 16,
                                  fontFamily: 'Urbanist',
                                  fontWeight: FontWeight.w600,
                                  height: 0,
                                  letterSpacing: -0.05,
                                ),
                              ),
                            ),
                            Positioned(
                              left: 326,
                              top: 0,
                              child: Text(
                                '129',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Color(0xFFD5C1B8),
                                  fontSize: 16,
                                  fontFamily: 'Urbanist',
                                  fontWeight: FontWeight.w600,
                                  height: 0,
                                  letterSpacing: -0.05,
                                ),
                              ),
                            ),
                            Positioned(
                              left: 74,
                              top: 0,
                              child: Text(
                                '127',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Color(0xFFD5C1B8),
                                  fontSize: 16,
                                  fontFamily: 'Urbanist',
                                  fontWeight: FontWeight.w600,
                                  height: 0,
                                  letterSpacing: -0.05,
                                ),
                              ),
                            ),
                            Positioned(
                              left: -6,
                              top: 0,
                              child: Text(
                                '126',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Color(0xFFD5C1B8),
                                  fontSize: 16,
                                  fontFamily: 'Urbanist',
                                  fontWeight: FontWeight.w600,
                                  height: 0,
                                  letterSpacing: -0.05,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
