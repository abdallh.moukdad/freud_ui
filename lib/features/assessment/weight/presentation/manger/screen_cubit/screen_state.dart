part of 'screen_cubit.dart';

sealed class ScreenState extends Equatable {
  const ScreenState();
}

final class ScreenInitial extends ScreenState {
  final initialWeightMeasure = 'kg';
  final initialWeightNumber = 70;

  @override
  List<Object> get props => [initialWeightMeasure, initialWeightNumber];
}

class ChangeWeightMeasure extends ScreenState {
  final weightMeasure;

  @override
  List<Object?> get props => [weightMeasure];

  ChangeWeightMeasure(this.weightMeasure);
}

class ChangeWeightNumber extends ScreenState {
  final weightNumber;

  @override
  List<Object?> get props => [weightNumber];

  ChangeWeightNumber(this.weightNumber);
}
