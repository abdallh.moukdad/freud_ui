import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'screen_state.dart';

class ScreenCubit extends Cubit<ScreenState> {
  ScreenCubit() : super(ScreenInitial());

  int weightNumber=5;
  var weightMeasure='kg';

  void changeWeight(var weight) {
    weightNumber = weight;
    emit(ChangeWeightNumber(weight));
  }

  void changeWeightMeasure(var measure) {
    weightMeasure = measure;
    emit(ChangeWeightMeasure(measure));
  }
}
