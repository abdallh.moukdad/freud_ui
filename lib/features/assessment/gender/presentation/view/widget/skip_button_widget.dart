import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mood_tracking_freud_ui/core/constants/constants.dart';

class SkipButton extends StatelessWidget {
  const SkipButton({super.key});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
        minimumSize: MaterialStateProperty.all(Size(343, 56)),
        maximumSize: MaterialStateProperty.all(Size(343, 56)),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(1000),
          ),
        ),
        backgroundColor: MaterialStateProperty.all(
          Color(0xFFE5EAD6),
        ),
      ),
      onPressed: () {

        Future<TimeOfDay?> selectedTime = showTimePicker(
          initialTime: TimeOfDay.now(),
          initialEntryMode: TimePickerEntryMode.dialOnly,
          context: context,
        );

      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Prefer to skip, thanks',
            style: TextStyle(
              color: Color(0xFF9BB067),
              fontSize: 18,
              fontFamily: 'Urbanist',
              fontWeight: FontWeight.w800,
              height: 0,
              letterSpacing: -0.07,
            ),
          ),
          const SizedBox(width: 12),
          SvgPicture.asset(KXIconPath)
        ],
      ),
    );
  }
}
