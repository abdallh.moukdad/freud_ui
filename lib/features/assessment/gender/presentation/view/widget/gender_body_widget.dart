import 'package:flutter/material.dart';
import 'package:mood_tracking_freud_ui/core/constants/constants.dart';
import 'package:mood_tracking_freud_ui/core/widgets/assessment_text_widget.dart';
import 'package:mood_tracking_freud_ui/core/widgets/primary_button.dart';
import 'package:mood_tracking_freud_ui/features/assessment/gender/presentation/view/widget/gender_type_widget.dart';
import 'package:mood_tracking_freud_ui/features/assessment/gender/presentation/view/widget/skip_button_widget.dart';

class GenderBody extends StatelessWidget {
  const GenderBody({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          AssessmentText(
            text: "What's your official gender?",
          ),

          SizedBox(height: 40,),
          GenderTypeContainer(
            text: "I am Male",
            icon: KManSymbolPath,
            image: KManPath,
          ),
          SizedBox(height: 20,),
          GenderTypeContainer(
            text: "I am Female",
            icon: KWomenSymbolPath,
            image: KWomenPath,
          ),
          SizedBox(height: 40,),
          SkipButton(),
          SizedBox(height: 16,),
          ContinueButton()
        ],
      ),
    );
  }
}
