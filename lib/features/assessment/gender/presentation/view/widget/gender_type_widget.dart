import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class GenderTypeContainer extends StatefulWidget {
  const GenderTypeContainer(
      {super.key, this.text, this.icon, this.image, this.mBorder});

  final text;
  final icon;
  final image;
  final mBorder;

  @override
  State<GenderTypeContainer> createState() => _GenderTypeContainerState();
}

class _GenderTypeContainerState extends State<GenderTypeContainer> {
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isSelected = !isSelected;
        });
      },
      child: Container(
        width: 385,
        // decoration: ShapeDecoration(
        //   color: Colors.white54,
        //   shape:OutlineInputBorder(
        //     borderRadius: BorderRadius.circular(39),
        //     // side: BorderSide.strokeAlignInside
        //   ),
        padding: const EdgeInsets.all(16),
        clipBehavior: Clip.antiAlias,
        decoration: ShapeDecoration(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            side: BorderSide(width: 1, color: Color(0xFF4E3321)),
            borderRadius: BorderRadius.circular(32),
          ),
          shadows: [
            isSelected
                ? BoxShadow(
                    color: Color(0x3F926247),
                    blurRadius: 0,
                    offset: Offset(0, 0),
                    spreadRadius: 4,
                  )
                : BoxShadow(
                    color: Color(0x0C4B3425),
                    blurRadius: 16,
                    offset: Offset(0, 8),
                    spreadRadius: 0,
                  )
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 150,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0),
                    child: Text(
                      widget.text,
                      style: TextStyle(
                        color: Color(0xFF3F3B35),
                        fontSize: 16,
                        fontFamily: 'Urbanist',
                        fontWeight: FontWeight.w800,
                        height: 0,
                        letterSpacing: -0.05,
                      ),
                    ),
                  ),
                  Spacer(),
                  SvgPicture.asset(widget.icon)
                ],
              ),
            ),
            Spacer(),
            SvgPicture.asset(widget.image)
          ],
        ),
      ),
    );
  }
}
