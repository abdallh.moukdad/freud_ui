import 'package:flutter/material.dart';
import 'package:mood_tracking_freud_ui/core/widgets/goal_app_bar.dart';
import 'package:mood_tracking_freud_ui/features/assessment/gender/presentation/view/widget/gender_body_widget.dart';

class GenderView extends StatelessWidget {
  const GenderView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      // child: MyAppBar(),
      child: SingleChildScrollView(
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Center(
              child: Container(
                constraints: BoxConstraints(
                  maxWidth: 400
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GoalAppBar(pageNumber: '2',),
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            // GoalBody(),
            GenderBody(),
          ],
        ),
      ),
    );
  }
}
